#include <iostream>

int main() {
   // Test that basic arithmetics still work:
   if( 1 + 1 == 2 ) {
      std::cout << "Yay!" << std::endl;
      return 0;
   } else {
      std::cout << "Nay." << std::endl;
      return 1;
   }
}
