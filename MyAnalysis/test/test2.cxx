// Local include(s):
#include "MyAnalysis/MyxAODAnalysis.h"

int main() {

   // Try to instantiate the analysis algorithm. This is more of a compile time check than a runtime one...
   MyxAODAnalysis alg( "Dummy", nullptr );

   return 0;
}

